#
# Be sure to run `pod lib lint ${POD_NAME}.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'MyUserPod'
  s.version          = '0.0.1'
  s.summary          = 'MyUserPod summary'
  s.description      = 'MyUserPod description'
  s.homepage         = 'https://gitee.com/yiqiesuiyuan/MyUserPod'
  s.author           = { 'zlcode' => '8@q.com' }
  s.source           = { :git => 'https://gitee.com/yiqiesuiyuan/MyUserPod.git', :tag => 'MyUserPod_0.0.1' }
  s.ios.deployment_target = '8.0'

  s.source_files = 'MyUserPod/Classes/MyUserPod.h'

  s.subspec 'Model' do |ss|
    ss.source_files = 'MyUserPod/Classes/Model/**/*'
  end

  s.subspec 'Controller' do |ss|
    ss.source_files = 'MyUserPod/Classes/Controller/**/*'
  end
  
  s.resources = ['MyUserPod/Assets/**/*']
  
  s.frameworks = 'UIKit', 'MapKit'
  
  s.dependency 'AFNetworking', '~> 2.3'
  s.dependency 'SVProgressHUD'
  s.dependency 'Masonry'

end
